/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.casino;

import org.casino.gui.comun.PanelCatalogoJuegos;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 *
 * @author xaelvil
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame ventana = new JFrame();
        ventana.setContentPane(new JScrollPane(new PanelCatalogoJuegos()));
        //ventana.setContentPane(new PanelIniciarSesion());
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setSize(400,400);
        ventana.setVisible(true);
    }

}
